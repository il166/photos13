package model;

public class tagSearch {

	private String tag1;
	private String tag2;
	private String operation;
	
	/**
	 * Empty constructor
	 */
	public tagSearch() {}
	
	/**
	 * Constructor for new tagSearch object
	 * @param tag1
	 * @param tag2
	 * @param operation
	 */
	public tagSearch(String tag1, String tag2, String operation) {
		this.tag1=tag1;
		this.tag2=tag2;
		this.operation=operation;
	}
	
	/**
	 * returns the private first tag
	 * @return tag1
	 */
	public String getTag1() {
		return tag1;
	}
	
	/**
	 * returns the private second tag
	 * @return tag2
	 */
	public String getTag2() {
		return tag2;
	}
	
	/**
	 * returns the private operation
	 * @return operation
	 */
	public String getOperation() {
		return operation;
	}
	
	/**
	 * sets tag1 to a new tag
	 * @param tag1
	 */
	public void setTag1(String tag1) {
		this.tag1=tag1;
	}
	
	/**
	 * sets tag2 to a new tag
	 * @param tag2
	 */
	public void setTag2(String tag2) {
		this.tag2=tag2;
	}
	
	/**
	 * sets operation to a new operation
	 * @param operation
	 */
	public void setOperation(String operation) {
		this.operation=operation;
	}
}
