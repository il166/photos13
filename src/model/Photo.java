package model;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;


import javax.imageio.ImageIO;

import javafx.fxml.FXML;
import javafx.collections.*;

import javafx.stage.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;



public class Photo implements Serializable{
	
	
	private transient ImageView image;
	private String imagePath;
	private String photoCaption;
	private LocalDate date;
	private ArrayList<Tag> photoTags;
	
	/**
	 * constructor for Photo
	 * @param imageView
	 * @param caption
	 * @param cdate
	 * @param tags
	 * @param albumName
	 * @throws IOException
	 */
	public Photo(ImageView imageView, String caption, LocalDate cdate, ArrayList<Tag> tags) throws IOException {
		if(imageView == null) {
			if(imagePath == null) {
				image = new ImageView();
			}
			else {
				image = new ImageView(new Image(new FileInputStream(imagePath.substring(5))));
			}
		}
		image = imageView;
		photoCaption = caption;
		date = cdate;
		photoTags = tags;
		
	}
	
	/**
	 * returns the private photo caption
	 * @return String photoCaption
	 */
	public String getCaption() {
		return photoCaption;
	}
	
	/**
	 * returns image from file path
	 * @return Image
	 */
	public Image getImage() {
	
		File imageFile = new File(imagePath);
		String fileLocation = imageFile.toURI().toString();
		Image fxImage = new Image(fileLocation);    
		return fxImage;

	}
	
	/**
	 * changes the private photoCaption to a new one
	 * @param newCaption
	 */
	public void changeCaption(String newCaption) {
		
		photoCaption = newCaption;
		
		
	}
	
	/**
	 * adds a tag to the photo
	 * @param tag
	 */
	public void addTag(Tag tag) {
		photoTags.add(tag);
		
	}
	
	/**
	 * removes a tag from the photo
	 * @param tagToRemove
	 */
	public void deleteTag(Tag tagToRemove) {
		
		photoTags.removeIf( tag-> tag.getKey().equals(tagToRemove.getKey()) && tag.getValue().equals(tagToRemove.getValue()));
	}
	
	/**
	 * gets the private arraylist of tags
	 * @return
	 */
	public ArrayList<Tag> getTags(){
		return this.photoTags;
	}
	
	/**
	 * returns the date of the photo
	 * @return date
	 */
	public LocalDate getDate() {
		
		return date;
	}
	
	/**
	 * returns the private imagePath string
	 * @return String imagePath
	 */
	public String getImagePath() {
		return this.imagePath;
	}
	
	/**
	 * sets a new image path
	 * @param s - new image path
	 */
	public void setImagePath(String s) {
		this.imagePath = s;
	}
	
	/**
	 * method to get and set a JavaFX Image from the image path
	 * @return Image 
	 * @throws IOException
	 */
	public Image getImageFromPath() throws IOException{
		if(imagePath != null) {
			File imageFile = new File(imagePath);
			String fileLocation = imageFile.toURI().toString();
			Image fxImage = new Image(fileLocation);    
			return fxImage;
		}
		else return null;
		
		
	}
		

}
