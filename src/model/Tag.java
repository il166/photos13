package model;

import java.io.Serializable;

public class Tag implements Serializable{

	private String key;
	private String value;
	
	/**
	 * Constructor for new tag
	 * @param key
	 * @param value
	 */
	public Tag(String key, String value) {
		this.key=key;
		this.value=value;
	}
	
	/**
	 * returns the private key
	 * @return key
	 */
	public String getKey() {
		return this.key;
	}
	
	/**
	 * returns the private value
	 * @return value
	 */
	public String getValue() {
		return this.value;
	}
}
