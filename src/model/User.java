
package model;

import java.io.*;
import java.util.*;
import javafx.*;

public class User implements Serializable{
	
	private String name;
	private ArrayList<PhotoAlbum> photoAlbumList;
	
	/**
	 * constructor for User, sets name and creates empty photoalbum arraylist
	 * @param name
	 */
	public User(String name) {
		this.name = name;
		photoAlbumList = new ArrayList<PhotoAlbum>();
	}
	
	/**
	 * returns private name
	 * @return String name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * returns private photo album list
	 * @return ArrayList<PhotoAlbum> photo album list
	 */
	public ArrayList<PhotoAlbum> getAlbumList(){
		return this.photoAlbumList;
	}
	
	/**
	 * adds an album to the photo album list
	 * @param album
	 */
	public void addAlbum(PhotoAlbum album) {
		this.photoAlbumList.add(album);
	}
	
	/**
	 * deletes an album from the photo album list
	 * @param album
	 */
	public void deleteAlbum(PhotoAlbum album) {
		this.photoAlbumList.remove(album);
	}

}
