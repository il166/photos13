package model;

import java.io.*;
import java.util.*;
import javafx.*;


public class PhotoAlbum implements Serializable{
	
	
	private ArrayList<Photo> photosInAlbum;
	private String photoAlbumName;
	private int numPhotosInAlbum;
	
	
	/**
	 * constructor for new empty photo album
	 * @param photo
	 * 
	 */
	public PhotoAlbum() {
		
		photosInAlbum = new ArrayList<Photo>();
		photoAlbumName = "";
		numPhotosInAlbum = 0;
		
	}
	
	/**
	 * constructor for new non-empty photo album
	 * @param photoList - list of photos
	 * @param name - phot album name
	 */
	public PhotoAlbum(ArrayList<Photo> photoList, String name) {
		
		photosInAlbum = photoList;
		photoAlbumName = name;
		numPhotosInAlbum = photoList.size();
		
	}
	

	/**
	 * get album name
	 * @return private name
	 */
	public String getName() {
		return photoAlbumName;
	}
	
	/**
	 * set new name for photo album
	 * @param newName
	 */
	public void setName(String newName) {
		photoAlbumName = newName;
	}
	
	
	/**
	 * get number of photos in album
	 * @return
	 */
	public int getAlbumNum() {
		return numPhotosInAlbum;
	}
	
	/**
	 * add photo to album
	 * @param photo
	 * @return
	 */
	public void addPhotoToAlbum(Photo photo){
		numPhotosInAlbum++;
		photosInAlbum.add(photo);
	
		
	}
	
	
	
	/**
	 * removes a photo from album
	 * @param photo
	 * @return
	 */
	public void removePhotoFromAlbum(Photo photo){
		numPhotosInAlbum--;
		photosInAlbum.remove(photo);
		
		
	}
	
	/**
	 * returns private ArrayList of photos
	 * @return ArrayList of photos
	 */
	public ArrayList<Photo> getPhotosInAlbum(){
		return photosInAlbum;
	}
	
	
	
	
	
	
	
	

}
