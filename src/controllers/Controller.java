//Itamar Levi, Young Choi

package controllers;

import java.util.*;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;
import model.*;
import javafx.event.*;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

import java.util.Base64.*;
import java.awt.Insets;
import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;



public class Controller implements Serializable{

	public static final String storeDir = "src/photos/";
	public static final String storeFile = "photoAlbums.ser"; 
	public static final String userFile = "docs/Users.ser";

	boolean move = false;

	public ListView<String> albumListView;
	public ListView<String> photoListView;
	public ListView<String> tagListView;
	public ListView<String> userListView;

	private ObservableList<String> obsAlbumList;
	private ObservableList<String> obsPhotoList;
	private ObservableList<String> obsUserList;
	private ObservableList<String> obsTagList;



	//PHOTO ALBUM FXML SHIT
	public Label userNameLabel = new Label();
	public Label photoAlbumsLabel = new Label();
	public Label albumNameLabel = new Label();
	public Label numPhotosLabel = new Label();
	public Label dateRangeLabel = new Label();
	public Button createAlbumButton = new Button();
	public Button deleteAlbumButton = new Button();
	public Button renameAlbumButton = new Button();
	public Button openAlbumButton = new Button();
	public Label albumNameInAlbumLabel = new Label();
	public TextField renameTextField = new TextField();
	public Button saveRenameButton = new Button();
	public Button searchTagButton = new Button();
	public Button searchDateButton = new Button();
	public Button cancelRenameButton = new Button();



	//PHOTO VIEW FXML SHIT
	public Label captionLabel = new Label();
	public ImageView photoImage = new ImageView();
	public Button editCaptionButton = new Button();
	public Button playSlideshowButton = new Button();
	public Button addPhotoButton = new Button();
	public Button deletePhotoButton = new Button();
	public Button copyPhotoButton = new Button();
	public Button movePhotoButton = new Button();
	public Button addTagButton = new Button();
	public Button deleteTagButton = new Button();
	public Button backToAlbumsButton = new Button();
	public Label dateLabel = new Label();
	public Button makeAlbumFromPhotos = new Button();





	//SLIDESHOW SHIT
	public ImageView slideshowImage = new ImageView();
	public Button previousPhoto = new Button();
	public Button nextPhoto = new Button();
	public Button exitSlideshow = new Button();
	public Label slideshowAlbumLabel = new Label();



	//create photo page
	public Button createPhotoButton = new Button();
	public Button chooseImageButton = new Button();
	public TextField enterCaption = new TextField();
	public TextField enterTags = new TextField();
	public Button cancelCreatePhotoButton = new Button();
	public ImageView uploadedImage = new ImageView();




	//login button
	public Button loginButton = new Button();
	//logout button
	public Button logoutButton = new Button();

	//login user name
	public TextField userNameInLogin = new TextField();

	//go to sign up page
	public Button toSignUpButton = new Button();

	//sign up
	public Button SignUpButton = new Button();
	public Button signUpCancel = new Button();
	public TextField userNameInSignUp = new TextField();

	//ADMIN Page
	public Button createUser = new Button();
	public Button createConfirm = new Button();
	public Button deleteUser = new Button();


	ArrayList<PhotoAlbum> albumList = new ArrayList<PhotoAlbum>();
	static ArrayList<Photo> photoList = new ArrayList<Photo>();
	static ArrayList<Tag> tagsInPhoto;
	ArrayList<String> tagList = new ArrayList<String>();
	static ArrayList<User> userList = new ArrayList<User>();
	ArrayList<String> userNameList = new ArrayList<String>();
	ArrayList<Photo> filteredPhotos = new ArrayList<Photo>();

	static PhotoAlbum selectedPhotoAlbum;
	static Photo selectedPhoto;
	static User selectedUser;

	private Stage stage;


	/**
	 * Initialize the login (initial) page
	 * @param primaryStage
	 * @throws Exception
	 */
	public void initialize(Stage primaryStage) throws Exception{


		this.stage=primaryStage;

		readUsers();
		//add Admin if not exists
		if(!userList.stream().anyMatch(user -> user.getName().equals("admin")) ) {
			User admin= new User("admin");
			userList.add(admin);
		}
		//add stock account if not exists
		if(!userList.stream().anyMatch(user -> user.getName().equals("stock")) ) {
			User stock= new User("stock");
			userList.add(stock);
		}

		openAlbumButton.setOnAction(arg0 -> {
			try {
				toSignUp(arg0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}


	/**
	 * login button action
	 * @param event
	 * @throws Exception
	 */
	public void login(ActionEvent event) throws Exception{
		String userName= userNameInLogin.getText();

		for(User u : userList) {
			if(u.getName().equals(userName)) {
				selectedUser = u;
				break;
			}
		}
		if(selectedUser == null) {
			System.out.println("SELECTED USER NULL");
		}
		else {
			//System.out.println("Selected User: " + selectedUser.getName());
		}

		if(userName.equals("admin")) {

			toAdmin(event);
			return;
		}
		if(userList.size()>0 && userList.stream().anyMatch(user -> user.getName().equals(userName)) ) {
			if(userName.equals("stock") && selectedUser.getAlbumList().size()==0) {
				try {
					selectedUser.getAlbumList().clear();

					ArrayList<Tag> photo1Tags = new ArrayList<Tag>();
					photo1Tags.add(new Tag("object","bunny"));
					photo1Tags.add(new Tag("background","brick"));
					long ldate=new File("stock/image1.jpg").lastModified();
					Date ddate=new Date(ldate);
					LocalDate date=ddate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

					Photo photo1 = new Photo(new ImageView(new Image(new FileInputStream("stock/image1.jpg"))), "Stock Photo #1", date, photo1Tags);


					ArrayList<Tag> photo2Tags = new ArrayList<Tag>();
					photo2Tags.add(new Tag("pixel","true"));
					photo2Tags.add(new Tag("person","lincoln"));
					long ldate2=new File("stock/image2.jpg").lastModified();
					Date ddate2=new Date(ldate2);
					LocalDate date2=ddate2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					Photo photo2 = new Photo(new ImageView(new Image(new FileInputStream("stock/image2.jpg"))), "Stock Photo #2", date2, photo2Tags);

					ArrayList<Tag> photo3Tags = new ArrayList<Tag>();
					photo3Tags.add(new Tag("color","blue"));
					photo3Tags.add(new Tag("pixel","true"));
					long ldate3=new File("stock/image3.jpeg").lastModified();
					Date ddate3=new Date(ldate3);
					LocalDate date3=ddate3.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					Photo photo3 = new Photo(new ImageView(new Image(new FileInputStream("stock/image3.jpeg"))), "Stock Photo #3", date3, photo3Tags);

					ArrayList<Tag> photo4Tags = new ArrayList<Tag>();
					long ldate4=new File("stock/image4.jpg").lastModified();
					Date ddate4=new Date(ldate4);
					LocalDate date4=ddate4.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					Photo photo4 = new Photo(new ImageView(new Image(new FileInputStream("stock/image4.jpg"))), "Stock Photo #4", date4, photo4Tags);

					ArrayList<Tag> photo5Tags = new ArrayList<Tag>();
					photo5Tags.add(new Tag("color","red"));
					photo5Tags.add(new Tag("background","brick"));
					long ldate5=new File("stock/image5.jpeg").lastModified();
					Date ddate5=new Date(ldate5);
					LocalDate date5=ddate5.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					Photo photo5 = new Photo(new ImageView(new Image(new FileInputStream("stock/image5.jpeg"))), "Stock Photo #5", date5, photo5Tags);



					photo1.setImagePath("stock/image1.jpg");
					photo2.setImagePath("stock/image2.jpg");
					photo3.setImagePath("stock/image3.jpeg");
					photo4.setImagePath("stock/image4.jpg");
					photo5.setImagePath("stock/image5.jpeg");


					ArrayList<Photo> stockPhotosList = new ArrayList<Photo>();
					stockPhotosList.add(photo1);
					stockPhotosList.add(photo2);
					stockPhotosList.add(photo3);
					stockPhotosList.add(photo4);
					stockPhotosList.add(photo5);

					PhotoAlbum stockAlbum = new PhotoAlbum(stockPhotosList, "stock");
					selectedUser.addAlbum(stockAlbum);
				} catch (FileNotFoundException e) {
					System.out.println("File not found");
					return;
				}
			}
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/NonAdminAlbums.fxml"));
			Parent newView = (Parent)loader.load();
			Controller controller = loader.getController();
			controller.openAlbumList(Photos.primaryStage);
			Scene newScene = new Scene(newView);

			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

			window.setScene(newScene);
			window.show();
		}else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Invalid Username");
			alert.setHeaderText("User not found.");
			alert.showAndWait();
			return;
		}

	}

	/**
	 * sets the list of users
	 * @param primarystage
	 */
	public void setUserList(Stage primarystage) {

		userList.forEach( user -> userNameList.add(user.getName()));
		obsUserList=FXCollections.observableArrayList(userNameList);   	
		Collections.sort(obsUserList);
		userListView.setItems(obsUserList);


	}

	/**
	 * Switches scene to SignUp Page
	 * @param event
	 * @throws IOException
	 */
	public void toSignUp(ActionEvent event) throws IOException {

		Parent newView = FXMLLoader.load(getClass().getResource("../view/SignUp.fxml"));
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();

	}

	/**
	 * Switches to Login Page
	 * @param event
	 * @throws IOException
	 */
	public void toLogin(ActionEvent event) throws IOException{

		Parent newView = FXMLLoader.load(getClass().getResource("../view/LogInPage.fxml"));
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();
	}

	/**
	 * Creates a user if it is not already present. 
	 * @param event
	 * @throws IOException
	 */
	public void createUser(ActionEvent event) throws IOException {
		String userName =userNameInSignUp.getText();
		User newUser= new User(userName);
		if(userList.size()>0 && userList.stream().anyMatch(user -> user.getName().equals(userName)) ) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Duplicate User");
			alert.setHeaderText("That username already exists.");
			alert.showAndWait();
			userNameInSignUp.setText("");
			return;
		}
		userList.add(newUser); 
		userNameList.add(userName);
		userNameInLogin.setText(userName);
		toLogin(event);
	}

	/**
	 * Navigates to the create user page from the admin page.
	 * @param event
	 * @throws IOException
	 */
	public void toCreateUserFromAdmin(ActionEvent event) throws IOException{
		Parent newView = FXMLLoader.load(getClass().getResource("../view/createUser.fxml"));
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();

	}

	/**
	 * Create user functionality for the Admin
	 * @param event
	 * @throws IOException
	 */
	public void createUserFromAdmin(ActionEvent event) throws IOException {
		String userName =userNameInSignUp.getText();
		User newUser= new User(userName);
		if(userList.size()>0 && userList.stream().anyMatch(user -> user.getName().equals(userName)) ) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Duplicate User");
			alert.setHeaderText("That username already exists.");
			alert.showAndWait();
			userNameInSignUp.setText("");
			return;
		}
		userList.add(newUser); 
		userNameList.add(userName);
		toAdmin(event);
		return;
	}

	/**
	 * Navigates to the Admin Page
	 * @param event
	 * @throws IOException
	 */
	public void toAdmin(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Admin Page.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.setUserList(Photos.primaryStage);
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();
	}

	/**
	 * deletes a user.
	 */
	public void deleteUser() {

		String userToRemove = userListView.getSelectionModel().getSelectedItem();
		userNameList.remove(userToRemove);
		obsUserList.remove(userToRemove);
		userList.removeIf(user -> user.getName()==userToRemove);		
		//remove from obs list 
		//user name list
		//user list
	}



	//-------------------------------------------PHOTO ALBUM LIST----------------------------------------------------------

	/**
	 * Initializes the NonAdminAlbums.fxml page and elements
	 * @param primaryStage
	 * @throws Exception
	 */
	public void openAlbumList(Stage primaryStage) throws Exception{

		ArrayList<String> photoAlbumsList = new ArrayList<String>();

		albumList = selectedUser.getAlbumList();

		for(PhotoAlbum pa : albumList) {
			photoAlbumsList.add(pa.getName());
		}

		//System.out.println("Album size: " + albumList.size());
		obsAlbumList=FXCollections.observableArrayList(photoAlbumsList);



		Collections.sort(obsAlbumList);

		albumListView.setItems(obsAlbumList);
		albumListView.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) -> showAlbumItem(primaryStage)); 
		albumListView.getSelectionModel().select(0);


		userNameLabel.setText(selectedUser.getName());

		searchTagButton.setOnAction(arg0 -> {
			try {
				searchByTags(arg0);
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		});

		//		searchTagButton.setOnAction(arg0 -> {
		//			try {
		//				searchByDates(arg0);
		//			} catch (IOException e2) {
		//				// TODO Auto-generated catch block
		//				e2.printStackTrace();
		//			}
		//		});
		searchDateButton.setOnAction(arg0 -> {
			try {
				searchByDates(arg0);
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		});

		logoutButton.setOnAction(arg0 -> {
			try {
				toLogin(arg0);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		renameAlbumButton.setOnAction(this::renameAlbum);
		createAlbumButton.setOnAction(this::createAlbum);
		deleteAlbumButton.setOnAction(this::deleteAlbum);
		openAlbumButton.setOnAction(arg0 -> {
			try {
				openAlbum(arg0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});



	}

	/**
	 * Show album details in album list page
	 * @param primaryStage
	 */
	private void showAlbumItem(Stage primaryStage) {

		albumNameLabel.setText(albumListView.getSelectionModel().getSelectedItem());
		for(PhotoAlbum pa : albumList) {
			try {
				if(albumNameLabel.getText().equals(pa.getName())) {
					selectedPhotoAlbum = pa;
					break;
				}
			}
			catch(NullPointerException e) {

			}
		}

		if(selectedPhotoAlbum == null) return;

		numPhotosLabel.setText(Integer.toString(selectedPhotoAlbum.getAlbumNum()));

		ArrayList<Photo> photosList=selectedPhotoAlbum.getPhotosInAlbum();
		ArrayList<LocalDate> dateList=new ArrayList<LocalDate>();



		photosList.forEach(photo ->
		dateList.add(photo.getDate())				
				);

		Comparator<LocalDate> comparison = new Comparator<LocalDate>() {
			public int compare(final LocalDate d1, final LocalDate d2) {
				return d1.compareTo(d2);
			}
		};
		Collections.sort(dateList, comparison);

		String fullMinDate="";
		String fullMaxDate="";
		if(dateList.size()>0) {
			fullMinDate=dateList.get(0).toString();
			fullMaxDate=dateList.get(dateList.size()-1).toString();
		}

		dateRangeLabel.setText("From: " + fullMinDate +"\nTo: "+fullMaxDate);


		//System.out.println(selectedPhotoAlbum.getName());

	}

	/**
	 * Rename button action
	 * @param event
	 */
	public void renameAlbum(ActionEvent event) {
		//System.out.println("In rename function");
		if(albumListView.getItems().size() == 0) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Unable to Perform Function");
			noAlbumAlert.setHeaderText("There are no albums selected to edit.");
			noAlbumAlert.showAndWait();
			return;
		}

		renameTextField.setVisible(true);
		albumNameLabel.setVisible(false);
		saveRenameButton.setVisible(true);
		cancelRenameButton.setVisible(true);


		saveRenameButton.setOnAction(new EventHandler<ActionEvent>(){


			public void handle(ActionEvent e) {

				if(renameTextField.getText().isEmpty()) {
					Alert noNameAlert = new Alert(AlertType.INFORMATION);
					noNameAlert.initOwner(Photos.primaryStage);
					noNameAlert.setTitle("Unable to Perform Function");
					noNameAlert.setHeaderText("Cannot rename to an empty name.");
					noNameAlert.showAndWait();
					return;
				}






				Alert confirmation = new Alert(AlertType.CONFIRMATION);
				confirmation.initOwner(Photos.primaryStage);
				confirmation.setTitle("Confirm?");
				confirmation.setHeaderText("Are you sure you want to rename this album?");
				ButtonType confirmRenameAlbum = new ButtonType("Rename");
				ButtonType confirmCancelEdit = new ButtonType("Cancel");
				confirmation.getButtonTypes().setAll(confirmRenameAlbum, confirmCancelEdit);


				Optional<ButtonType> result = confirmation.showAndWait();

				if(result.get() == confirmCancelEdit) {
					renameTextField.clear();
					renameTextField.setVisible(false);

					albumNameLabel.setVisible(true);
					saveRenameButton.setVisible(false);
					return;

				}
				int index = albumListView.getSelectionModel().getSelectedIndex();



				selectedPhotoAlbum.setName(renameTextField.getText().trim());
				albumNameLabel.setText(selectedPhotoAlbum.getName());

				obsAlbumList.set(index, albumNameLabel.getText());
				//Collections.sort(obsAlbumList);
				//int newIndex = albumListView.getItems().indexOf(albumNameLabel.getText());
				albumListView.getSelectionModel().select(index);

				albumNameLabel.setVisible(true);
				saveRenameButton.setVisible(false);
				renameTextField.setVisible(false);
				renameTextField.clear();
				cancelRenameButton.setVisible(false);

			}

		});
		
		cancelRenameButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				renameTextField.setVisible(false);
				albumNameLabel.setVisible(true);
				saveRenameButton.setVisible(false);
				renameTextField.clear();
				
				cancelRenameButton.setVisible(false);
			}
			
		});

	}

	/**
	 * Create album action
	 * @param event
	 */
	public void createAlbum(ActionEvent event) {

		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("New Photo Album");
		dialog.setHeaderText("Create a New Photo Album");
		dialog.setContentText("Please enter the new photo album name: ");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			if(albumList != null){
				for(PhotoAlbum pa : albumList) {

					if(pa.getName().equals(result.get().toLowerCase().trim())) {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.initOwner(Photos.primaryStage);
						alert.setTitle("Duplicate Album");
						alert.setHeaderText("You cannot add a duplicate album. Please try again");
						alert.showAndWait();
						createAlbum(event);
						return;
					}

				}
			}


			PhotoAlbum createdPhotoAlbum = new PhotoAlbum(new ArrayList<Photo>(), result.get().trim());
			//albumList.add(createdPhotoAlbum);
			selectedUser.addAlbum(createdPhotoAlbum);
			obsAlbumList.add(createdPhotoAlbum.getName());
			Collections.sort(obsAlbumList);
			int newIndex = albumListView.getItems().indexOf(createdPhotoAlbum.getName());
			albumListView.getSelectionModel().select(newIndex);


		}


	}

	/**
	 * Delete album action
	 * @param event
	 */
	public void deleteAlbum(ActionEvent event) {

		if(albumListView.getItems().size() == 0) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Unable to Perform Function");
			noAlbumAlert.setHeaderText("There are no albums selected to delete.");
			noAlbumAlert.showAndWait();
			return;
		}


		Alert confirmation = new Alert(AlertType.CONFIRMATION);
		confirmation.initOwner(Photos.primaryStage);
		confirmation.setTitle("Delete Photo Album?");
		confirmation.setHeaderText("Are you sure you want to delete this album?");

		ButtonType yesDelete = new ButtonType("Yes");
		ButtonType noDelete = new ButtonType("No");


		confirmation.getButtonTypes().setAll(yesDelete, noDelete);

		Optional<ButtonType> result = confirmation.showAndWait();
		if(result.get() == noDelete) return;

		int index = albumListView.getSelectionModel().getSelectedIndex();
		PhotoAlbum removedAlbum = null;
		for(PhotoAlbum pa : albumList) {
			if(albumListView.getSelectionModel().getSelectedItem().equals(pa.getName())) {
				removedAlbum = pa;
				break;

			}
		}
		//System.out.println(removedAlbum.getName());
		//first album
		if(index == 0) {

			//only one album in list
			if(albumListView.getItems().size() == 1) {
				selectedUser.deleteAlbum(removedAlbum);
				albumList.clear();
				albumListView.getItems().clear();
				albumNameLabel.setText("");
				numPhotosLabel.setText("");
				dateRangeLabel.setText("");
			}

			else {
				//albumListView.getSelectionModel().selectNext();

				selectedUser.deleteAlbum(removedAlbum);
				try {
					openAlbumList(Photos.primaryStage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}


		else {
			//albumListView.getSelectionModel().selectNext();

			selectedUser.deleteAlbum(removedAlbum);
			try {
				openAlbumList(Photos.primaryStage);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}




	}

	/**
	 * Open album action, switch to PhotosInAlbum.fxml
	 * @param event
	 * @throws IOException
	 */
	public void openAlbum(ActionEvent event) throws IOException {
		if(selectedUser.getAlbumList().size() == 0) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Unable to Perform Function");
			noAlbumAlert.setHeaderText("There are no photo albums selected to open.");
			noAlbumAlert.showAndWait();
			return;
		}

		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/PhotosInAlbum.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.initializePhotosInAlbum(Photos.primaryStage);
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();


	}

	/**
	 * Searched all albums for images with a specific tag or a combinaton of 2 tags.
	 * @param event
	 * @throws IOException
	 */
	public void searchByTags(ActionEvent event) throws IOException {
		Dialog<tagSearch> dialog = new Dialog<>();
		dialog.setTitle("Enter Tags");

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);

		TextField tag1 = new TextField();
		tag1.setPromptText("Tag 1");
		TextField tag2 = new TextField();
		tag2.setPromptText("Tag 2");

		gridPane.add(new Label("Please enter a tag.\nExample: person=lincoln (NO SPACES)\nOperation and Tag2 are for combinations.\n ---If one is present the other is required!"), 0, 0);
		gridPane.add(new Label("Tag 1"), 0, 1);
		gridPane.add(tag1, 1, 1);
		gridPane.add(new Label("Tag 2"), 0, 3);
		gridPane.add(tag2, 1, 3);

		String operations[] = 
			{ "and", "or" }; 

		ComboBox operationBox = new ComboBox(FXCollections.observableArrayList(operations));
		operationBox.setPromptText("Please choose an Operation");

		TextField optext = new TextField();

		gridPane.add(operationBox, 1, 2);


		dialog.getDialogPane().setContent(gridPane);

		Platform.runLater(() -> tag1.requestFocus());

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == loginButtonType) {
				return new tagSearch(tag1.getText(), tag2.getText(), (String)operationBox.getValue());
			}
			return null;
		});

		Optional<tagSearch> result = dialog.showAndWait();
		if(result.toString().equals("Optional.empty")) return;
		tagSearch searchParameter = new tagSearch();
		result.ifPresent(parameter -> {
			searchParameter.setTag1(parameter.getTag1());
			searchParameter.setTag2(parameter.getTag2());
			searchParameter.setOperation(parameter.getOperation());
		});

		ArrayList<PhotoAlbum> albumList = selectedUser.getAlbumList();
		ArrayList<Photo> photos = new ArrayList<Photo>(); 
		albumList.forEach(album -> {album.getPhotosInAlbum().forEach(photo->{
			photos.add(photo);
		});
		});
		
		if(searchParameter.getOperation()==null && !searchParameter.getTag2().equals("")) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Unable to Perform Search");
			noAlbumAlert.setHeaderText("Please enter valid search parameters...");
			noAlbumAlert.showAndWait();
			searchByTags(event);
			return;

		}
		//only searching by one tag and OR
		if(searchParameter.getOperation()==null || searchParameter.getOperation().equals("or") ) {

			String[] tagArr=searchParameter.getTag1().split("=");
			if(tagArr.length!=2) {
				Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
				noAlbumAlert.initOwner(Photos.primaryStage);
				noAlbumAlert.setTitle("Invalid Tag 1");
				noAlbumAlert.setHeaderText("Enter a valid tag if you want to search.");
				noAlbumAlert.showAndWait();
				searchByTags(event);
				return;
			}
			String key=tagArr[0];
			String val=tagArr[1];
			String key2;
			String val2;
			if(searchParameter.getTag2().equals("")) {
				key2="";
				val2="";
			}else {
				String[] tagArr2=searchParameter.getTag2().split("=");
				key2=tagArr2[0];
				val2=tagArr2[1];
				

			}
			photos.forEach(photo-> {
				ArrayList<Tag> tagList = photo.getTags();
				tagList.forEach(tag -> {
					if(((tag.getKey().equals(key) && tag.getValue().equals(val)) || (tag.getKey().equals(key2) && tag.getValue().equals(val2))) 
							&& !filteredPhotos.contains(photo)) {
						filteredPhotos.add(photo);						
					}
				});
			});

		}else {
			String[] tagArr=searchParameter.getTag1().split("=");
			String key=tagArr[0];
			String val=tagArr[1];
			String key2="";
			String val2="";
			if(searchParameter.getTag2().equals("")) {
				Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
				noAlbumAlert.initOwner(Photos.primaryStage);
				noAlbumAlert.setTitle("Second Tag cannot be empty");
				noAlbumAlert.setHeaderText("Please enter a Second Tag.");
				noAlbumAlert.showAndWait();
				searchByTags(event);
				return;
			}else {
				String[] tagArr2=searchParameter.getTag2().split("=");
				key2=tagArr2[0];
				val2=tagArr2[1];	
			}

			for(Photo photo : photos) {
				ArrayList<Tag> tagList = photo.getTags();
				boolean key1Present=false;
				boolean key2Present=false;
				for(Tag tag: tagList) {
					if((tag.getKey().equals(key) && tag.getValue().equals(val))){
						key1Present=true;
					}else if((tag.getKey().equals(key2) && tag.getValue().equals(val2))){
						key2Present=true;
					}

					if(key2Present && key1Present && !filteredPhotos.contains(photo)) {
						filteredPhotos.add(photo);
						break;
					}
				}
			}	
		}

		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/MakeAlbumFromSearch.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.initializePhotosInSearchPage(Photos.primaryStage,filteredPhotos);
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();






	}

	/**
	 * Searched all albums for images within a given date range.
	 * @param event
	 * @throws IOException
	 */
	public void searchByDates(ActionEvent event) throws IOException {
		Dialog<Pair<LocalDate, LocalDate>> dialog = new Dialog<>();
		dialog.setTitle("Search By Date");

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);

		DatePicker d = new DatePicker(); 
		DatePicker d2 = new DatePicker(); 

		gridPane.add(new Label("From"), 0, 1);
		gridPane.add(d, 0, 2);
		gridPane.add(new Label("To"), 0, 3);
		gridPane.add(d2, 0, 4);

		dialog.getDialogPane().setContent(gridPane);

		// Request focus on the username field by default.
		Platform.runLater(() -> d.requestFocus());

		// Convert the result to a username-password-pair when the login button is clicked.
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == loginButtonType) {
				return new Pair<>(d.getValue(), d2.getValue());
			}
			return null;
		});

		Optional<Pair<LocalDate, LocalDate>> result = dialog.showAndWait();
		if(result.toString().equals("Optional.empty")) return;
		LocalDate date1=result.get().getKey();
		LocalDate date2=result.get().getValue();
		
		if(date2==null || date1==null) {
			Alert dateAlert = new Alert(AlertType.INFORMATION);
			dateAlert.initOwner(Photos.primaryStage);
			dateAlert.setTitle("Invalid Dates Chosen");
			dateAlert.setHeaderText("Please ensure both dates are chosen.");
			dateAlert.showAndWait();
			searchByDates(event);
			return;

		}
		if(date2.isBefore(date1)) {
			Alert dateAlert = new Alert(AlertType.INFORMATION);
			dateAlert.initOwner(Photos.primaryStage);
			dateAlert.setTitle("Invalid Dates Chosen");
			dateAlert.setHeaderText("Please make sure the first date is before the second date.");
			dateAlert.showAndWait();
			searchByDates(event);
			return;
			
		}

		ArrayList<PhotoAlbum> albumList = selectedUser.getAlbumList();
		ArrayList<Photo> photos = new ArrayList<Photo>(); 
		albumList.forEach(album -> {album.getPhotosInAlbum().forEach(photo->{
			photos.add(photo);
		});
		});
		ArrayList<Photo> filteredPhotos = new ArrayList<Photo>();
		photos.forEach(photo -> {

			if(photo.getDate().compareTo(date1)>=0 && photo.getDate().compareTo(date2)<=0) {
				filteredPhotos.add(photo);
			}

		});

		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/MakeAlbumFromSearch.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.initializePhotosInSearchPage(Photos.primaryStage,filteredPhotos);
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();

	}
	//------------------------------------------------PHOTOS IN ALBUM--------------------------------------------------

	/**
	 * Initializes the photos in an album. Retrieves all photos in the selected album and gets the data for them.
	 * @param primaryStage
	 */
	public void initializePhotosInAlbum(Stage primaryStage) {
		ArrayList<String> photoCaptionList = new ArrayList<String>();

		photoList = selectedPhotoAlbum.getPhotosInAlbum();
		photoList.forEach(photo ->{

			photoCaptionList.add(photo.getCaption());
		});


		userNameLabel.setText(selectedUser.getName());

		obsPhotoList=FXCollections.observableArrayList(photoCaptionList);
		albumNameInAlbumLabel.setText(selectedPhotoAlbum.getName());




		//Collections.sort(obsPhotoList);


		photoListView.setItems(obsPhotoList);
		photoListView.setCellFactory(arg -> new ListCell<String>() {
			private ImageView i = new ImageView();
			@Override
			public void updateItem(String caption, boolean empty) {
				super.updateItem(caption, empty);
				if(empty) {
					setText(null);
					setGraphic(null);

				}
				else {
					for(Photo p : photoList) {
						if(p.getCaption().equals(caption)) {
							i.setImage(p.getImage());
							i.setFitHeight(80);
							i.setFitWidth(80);
							setText(p.getCaption());
							setGraphic(i);
						}
					}
				}
			}

		});


		photoListView.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) -> showPhotoItem(primaryStage)); 
		photoListView.getSelectionModel().select(0);




		logoutButton.setOnAction(arg0 -> {
			try {
				toLogin(arg0);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});

		addPhotoButton.setOnAction(arg0 -> {
			try {
				switchToAddPhoto(arg0);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		backToAlbumsButton.setOnAction(arg0 -> {
			try {
				backToAlbums(arg0);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		playSlideshowButton.setOnAction(arg0 -> {
			try {
				toSlideshow(arg0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});


		copyPhotoButton.setOnAction(arg0 -> {
			try {
				copyPhoto(arg0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		movePhotoButton.setOnAction(arg0 -> {
			try {
				movePhoto(arg0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		editCaptionButton.setOnAction(this::editCaption);

		addTagButton.setOnAction(this::addTag);

		deletePhotoButton.setOnAction(this::deletePhoto);

		deleteTagButton.setOnAction(this::deleteTag);
	}

	/**
	 *Displays the data and information for a photo in the list view. 
	 * @param primaryStage
	 */
	private void showPhotoItem(Stage primaryStage) {
		if(photoList.size()==0)
			return;
		selectedPhoto = photoList.get(photoListView.getSelectionModel().getSelectedIndex());

		if(selectedPhoto == null) return;

		captionLabel.setText(selectedPhoto.getCaption());
		dateLabel.setText(selectedPhoto.getDate().toString());

		photoImage.setImage(selectedPhoto.getImage());

		tagsInPhoto = selectedPhoto.getTags();

		tagList= tagsInPhoto.stream().map( tag ->{
			String key = tag.getKey();
			String value = tag.getValue();
			String tagToDisplay = key+" : " + value;
			return tagToDisplay;
		}).collect(ArrayList::new, ArrayList::add,ArrayList::addAll);

		obsTagList=FXCollections.observableArrayList(tagList);   	
		Collections.sort(obsTagList);
		tagListView.setItems(obsTagList);

		photoImage.setPreserveRatio(true);

	}

	/**
	 * Allows user to edit captions.
	 * @param event
	 */
	private void editCaption(ActionEvent event) {
		if(selectedPhotoAlbum.getAlbumNum() == 0) {
			Alert noPhotoAlert = new Alert(AlertType.INFORMATION);
			noPhotoAlert.initOwner(Photos.primaryStage);
			noPhotoAlert.setTitle("No Photo Uploaded");
			noPhotoAlert.setHeaderText("Please upload a photo in order to rename it.");
			noPhotoAlert.showAndWait();
			return;
		}
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Edit Photo Caption");
		dialog.setHeaderText("Enter a new caption: ");


		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {

			selectedPhoto.changeCaption(result.get());
			obsPhotoList.set(photoListView.getSelectionModel().getSelectedIndex(), selectedPhoto.getCaption());
			captionLabel.setText(selectedPhoto.getCaption());
			photoListView.getSelectionModel().select(photoListView.getSelectionModel().getSelectedIndex());
			//showPhotoItem(Photos.primaryStage);
		}

	}
	/**
	 * Allows users to move photos from one album to another.
	 * @param event
	 */
	private void movePhoto(ActionEvent event) throws IOException {
		if(selectedPhotoAlbum.getAlbumNum() == 0) {
			Alert noPhotoAlert = new Alert(AlertType.INFORMATION);
			noPhotoAlert.initOwner(Photos.primaryStage);
			noPhotoAlert.setTitle("No Photo Uploaded");
			noPhotoAlert.setHeaderText("Please upload a photo in order to move it.");
			noPhotoAlert.showAndWait();
			return;
		}

		if(selectedUser.getAlbumList().size() == 1) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Cannot Move Photo");
			noAlbumAlert.setHeaderText("You only have one photo album. Please create another photo album in order to copy this photo.");
			noAlbumAlert.showAndWait();
			return;
		}
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle("Move Photo");

		// Set the button types.
		ButtonType copyButtonType = new ButtonType("Move", ButtonData.OK_DONE);
		ButtonType cancelCopy = new ButtonType("Cancel");
		dialog.getDialogPane().getButtonTypes().addAll(copyButtonType, cancelCopy);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);

		ComboBox<String> moveAlbums = new ComboBox<String>();
		for(PhotoAlbum pa : selectedUser.getAlbumList()) {
			if(!pa.equals(selectedPhotoAlbum)) moveAlbums.getItems().add(pa.getName());
		}
		moveAlbums.setPromptText("Choose an album...");

		gridPane.add(new Label("Choose the album to move to:"), 0, 0);
		gridPane.add(moveAlbums, 1, 0);


		dialog.getDialogPane().setContent(gridPane);

		Optional<String> result = dialog.showAndWait();

		if(result.isPresent() && moveAlbums.getValue() != null) {
			Alert confirmation = new Alert(AlertType.CONFIRMATION);
			confirmation.initOwner(Photos.primaryStage);
			confirmation.setTitle("Confirm?");
			confirmation.setHeaderText("Are you sure you want to move this photo?");
			ButtonType confirmMove = new ButtonType("Yes");
			ButtonType cancel = new ButtonType("No");
			confirmation.getButtonTypes().setAll(confirmMove, cancel);
			Optional<ButtonType> r = confirmation.showAndWait();

			if(r.get() == cancel) {

				movePhoto(event);
				return;


			}

			else {
				for(PhotoAlbum pa : selectedUser.getAlbumList()) {
					if(pa.getName().equals(moveAlbums.getValue())) {
						System.out.println("Selected photo: " + selectedPhoto.getTags().size());
						ArrayList<Tag> newTags = new ArrayList<Tag>();
						newTags.addAll(selectedPhoto.getTags());
						Photo newPhoto = new Photo(new ImageView(selectedPhoto.getImage()), selectedPhoto.getCaption(), selectedPhoto.getDate(), newTags);
						newPhoto.setImagePath(selectedPhoto.getImagePath());
						pa.addPhotoToAlbum(newPhoto);
						move = true;
						deletePhoto(event);
						break;
					}
				}



			}

		}
		else {
			Alert info = new Alert(AlertType.INFORMATION);
			info.initOwner(Photos.primaryStage);
			info.setTitle("Choose an Album");
			info.setHeaderText("Please choose an album, or cancel.");
			info.showAndWait();
			movePhoto(event);
		}

	}

	/**
	 * Allows users to copy photos from one album to another.
	 * @param event
	 */
	private void copyPhoto(ActionEvent event) throws IOException {
		if(selectedPhotoAlbum.getAlbumNum() == 0) {
			Alert noPhotoAlert = new Alert(AlertType.INFORMATION);
			noPhotoAlert.initOwner(Photos.primaryStage);
			noPhotoAlert.setTitle("No Photo Uploaded");
			noPhotoAlert.setHeaderText("Please upload a photo in order to copy it.");
			noPhotoAlert.showAndWait();
			return;
		}

		if(selectedUser.getAlbumList().size() == 1) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Cannot Copy Photo");
			noAlbumAlert.setHeaderText("You only have one photo album. Please create another photo album in order to copy this photo.");
			noAlbumAlert.showAndWait();
			return;
		}
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle("Copy Photo");

		// Set the button types.
		ButtonType copyButtonType = new ButtonType("Copy", ButtonData.OK_DONE);
		ButtonType cancelCopy = new ButtonType("Cancel");
		dialog.getDialogPane().getButtonTypes().addAll(copyButtonType, cancelCopy);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);

		ComboBox<String> copyAlbums = new ComboBox<String>();
		for(PhotoAlbum pa : selectedUser.getAlbumList()) {
			if(!pa.equals(selectedPhotoAlbum)) copyAlbums.getItems().add(pa.getName());
		}
		copyAlbums.setPromptText("Choose an album...");

		gridPane.add(new Label("Choose the album to copy to:"), 0, 0);
		gridPane.add(copyAlbums, 1, 0);


		dialog.getDialogPane().setContent(gridPane);

		Optional<String> result = dialog.showAndWait();

		if(result.isPresent() && copyAlbums.getValue() != null) {
			Alert confirmation = new Alert(AlertType.CONFIRMATION);
			confirmation.initOwner(Photos.primaryStage);
			confirmation.setTitle("Confirm?");
			confirmation.setHeaderText("Are you sure you want to copy this photo?");
			ButtonType confirmCopy = new ButtonType("Yes");
			ButtonType cancel = new ButtonType("No");
			confirmation.getButtonTypes().setAll(confirmCopy, cancel);
			Optional<ButtonType> r = confirmation.showAndWait();

			if(r.get() == cancel) {

				copyPhoto(event);
				return;


			}

			else {
				for(PhotoAlbum pa : selectedUser.getAlbumList()) {
					if(pa.getName().equals(copyAlbums.getValue())) {
						ArrayList<Tag> newTags = new ArrayList<Tag>();
						newTags.addAll(selectedPhoto.getTags());
						Photo newPhoto = new Photo(new ImageView(selectedPhoto.getImage()), selectedPhoto.getCaption(), selectedPhoto.getDate(), newTags);
						newPhoto.setImagePath(selectedPhoto.getImagePath());
						pa.addPhotoToAlbum(newPhoto);
						break;
					}
				}



			}

		}
		else {
			Alert info = new Alert(AlertType.INFORMATION);
			info.initOwner(Photos.primaryStage);
			info.setTitle("Choose an Album");
			info.setHeaderText("Please choose an album, or cancel.");
			info.showAndWait();
			copyPhoto(event);
		}

	}

	/**
	 * Adds photos to the selected album. 
	 * @param primaryStage
	 * @throws IOException
	 */
	private void addPhoto(Stage primaryStage) throws IOException{

		FileChooser fileChooser = new FileChooser();


		chooseImageButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent e){
				File imageFile = fileChooser.showOpenDialog(stage);
				if(imageFile != null) {

					Image i = new Image(imageFile.toURI().toString());
					uploadedImage.setImage(i);


				}
			}
		});

		createPhotoButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent e){
				if(uploadedImage.getImage() == null) {
					Alert noPhotoAlert = new Alert(AlertType.INFORMATION);
					noPhotoAlert.initOwner(Photos.primaryStage);
					noPhotoAlert.setTitle("No Photo Uploaded");
					noPhotoAlert.setHeaderText("Please choose a photo in order to upload.");
					noPhotoAlert.showAndWait();
					return;
				}
				Alert confirmation = new Alert(AlertType.CONFIRMATION);
				confirmation.initOwner(Photos.primaryStage);
				confirmation.setTitle("Confirm?");
				confirmation.setHeaderText("Are you sure you want to upload this photo?");
				ButtonType confirmUpload = new ButtonType("Yes");
				ButtonType cancelUpload = new ButtonType("No");
				confirmation.getButtonTypes().setAll(confirmUpload, cancelUpload);
				Optional<ButtonType> result = confirmation.showAndWait();

				if(result.get() == cancelUpload) {


					try {
						addPhoto(Photos.primaryStage);

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}



				}

				else {

					Photo newPhoto;
					String newCaption = enterCaption.getText();

					long ldate=new File(uploadedImage.getImage().getUrl().substring(5)).lastModified();
					Date ddate=new Date(ldate);
					LocalDate date=ddate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

					String tag=enterTags.getText();
					ArrayList<Tag> tags = new ArrayList<Tag>();
					
					String[] tagsInTF=tag.split(",");
					String[] tagArr;
					if(!tag.equals("")){
						for(String s : tagsInTF) {
							tagArr = s.split("=");
							
								if(tagArr.length != 2) {
									Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
									noAlbumAlert.initOwner(Photos.primaryStage);
									noAlbumAlert.setTitle("Invalid Tag 1");
									noAlbumAlert.setHeaderText("");
									noAlbumAlert.showAndWait();
									try {
										addPhoto(stage);
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									return;
								}
								String key=tagArr[0];
								String val=tagArr[1];
								tags.add(new Tag(key,val));
						}
					}
					
					
					try {
						newPhoto = new Photo(uploadedImage, newCaption, date, tags);
						newPhoto.setImagePath(uploadedImage.getImage().getUrl().substring(5));
						//System.out.println("image path: " + newPhoto.getImagePath());
						selectedPhotoAlbum.addPhotoToAlbum(newPhoto);


					} catch (IOException e3) {
						// TODO Auto-generated catch block
						e3.printStackTrace();	
					}


					try {
						openAlbum(e);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}


			}
		});

		cancelCreatePhotoButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent e) {

				enterCaption.clear();
				enterTags.clear();
				uploadedImage.setImage(null);

				try {
					openAlbum(e);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}



			}
		});

	}

	/**
	 * Switches to the add photo page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToAddPhoto(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/CreatePhoto.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.addPhoto(Photos.primaryStage);
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();

	}

	/**
	 * navigates back to the album list.
	 * @param event
	 * @throws Exception
	 */
	private void backToAlbums(ActionEvent event) throws Exception{
		selectedPhoto=null;
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/NonAdminAlbums.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.openAlbumList(Photos.primaryStage);
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();
	}

	/**
	 * Deletes photo from an album
	 * @param event
	 */
	private void deletePhoto(ActionEvent event){


		if(photoListView.getItems().size() == 0) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Unable to Perform Function");
			noAlbumAlert.setHeaderText("There are no photos selected to delete.");
			noAlbumAlert.showAndWait();
			return;
		}

		if(move) {
			move = false;
			int index = photoListView.getSelectionModel().getSelectedIndex();
			Photo removedPhoto = photoList.get(index);
			//first album
			if(index == 0) {

				//only one album in list
				if(photoListView.getItems().size() == 1) {
					selectedPhotoAlbum.removePhotoFromAlbum(removedPhoto);
					photoList.clear();
					photoListView.getItems().clear();
					captionLabel.setText("");
					dateLabel.setText("");
					dateRangeLabel.setText("");
					photoImage.setImage(null);
					tagListView.getItems().clear();
				}

				else {
					photoListView.getSelectionModel().selectNext();
					selectedPhotoAlbum.removePhotoFromAlbum(removedPhoto);
					//albumList.remove(index);
					obsPhotoList.remove(index);
					showPhotoItem(Photos.primaryStage);
				}

			}


			else {
				selectedPhotoAlbum.removePhotoFromAlbum(removedPhoto);
				obsPhotoList.remove(index);
				//listView.getItems().remove(index);
				photoListView.getSelectionModel().select(index);
				showPhotoItem(Photos.primaryStage);

			}


		}
		else{
			Alert confirmation = new Alert(AlertType.CONFIRMATION);

			confirmation.initOwner(Photos.primaryStage);
			confirmation.setTitle("Delete Photo?");
			confirmation.setHeaderText("Are you sure you want to delete this photo?");

			ButtonType yesDelete = new ButtonType("Yes");
			ButtonType noDelete = new ButtonType("No");


			confirmation.getButtonTypes().setAll(yesDelete, noDelete);

			Optional<ButtonType> result = confirmation.showAndWait();
			if(result.get() == noDelete) {
				return;
			}

			int index = photoListView.getSelectionModel().getSelectedIndex();
			Photo removedPhoto = photoList.get(index);
			//first album
			if(index == 0) {

				//only one album in list
				if(photoListView.getItems().size() == 1) {
					selectedPhotoAlbum.removePhotoFromAlbum(removedPhoto);
					photoList.clear();
					photoListView.getItems().clear();
					captionLabel.setText("");
					dateLabel.setText("");
					dateRangeLabel.setText("");
					photoImage.setImage(null);
					tagListView.getItems().clear();
				}

				else {
					photoListView.getSelectionModel().selectNext();
					selectedPhotoAlbum.removePhotoFromAlbum(removedPhoto);
					//albumList.remove(index);
					obsPhotoList.remove(index);
					showPhotoItem(Photos.primaryStage);
				}

			}


			else {
				selectedPhotoAlbum.removePhotoFromAlbum(removedPhoto);
				obsPhotoList.remove(index);
				//listView.getItems().remove(index);
				photoListView.getSelectionModel().select(index);
				showPhotoItem(Photos.primaryStage);

			}

		}


	}

	/**
	 * Adds tag to a photo
	 * @param event
	 */
	public void addTag(ActionEvent event) {
		
		if(photoListView.getItems().size() == 0) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Unable to Perform Function");
			noAlbumAlert.setHeaderText("There are not photos to add a tag to..");
			noAlbumAlert.showAndWait();
			return;
		}
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle("Add Tags");

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);

		TextField key = new TextField();
		key.setPromptText("Key");
		TextField value = new TextField();
		value.setPromptText("Value");

		gridPane.add(new Label("Key:"), 0, 0);
		gridPane.add(key, 1, 0);
		gridPane.add(new Label("Value:"), 2, 0);
		gridPane.add(value, 3, 0);

		dialog.getDialogPane().setContent(gridPane);

		// Request focus on the username field by default.
		Platform.runLater(() -> key.requestFocus());

		// Convert the result to a username-password-pair when the login button is clicked.
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == loginButtonType) {
				return new Pair<>(key.getText(), value.getText());
			}
			return null;
		});

		Optional<Pair<String, String>> result = dialog.showAndWait();

		result.ifPresent(pair -> {
			Tag newTag = new Tag(pair.getKey(), pair.getValue());
			selectedPhoto.addTag(newTag);
			tagsInPhoto=selectedPhoto.getTags();
		});
		tagList= tagsInPhoto.stream().map( tag ->{
			String key2 = tag.getKey();
			String value2 = tag.getValue();
			String tagToDisplay = key2+" : " + value2;
			return tagToDisplay;
		}).collect(ArrayList::new, ArrayList::add,ArrayList::addAll);

		obsTagList=FXCollections.observableArrayList(tagList);   	
		Collections.sort(obsTagList);
		tagListView.setItems(obsTagList);

	}

	/**
	 * deletes tag from a photo
	 * @param event
	 */
	public void deleteTag(ActionEvent event) {
		
		if(photoListView.getItems().size() == 0) {
			Alert noAlbumAlert = new Alert(AlertType.INFORMATION);
			noAlbumAlert.initOwner(Photos.primaryStage);
			noAlbumAlert.setTitle("Unable to Perform Function");
			noAlbumAlert.setHeaderText("There are not photos to delete a tag from.");
			noAlbumAlert.showAndWait();
			return;
		}
		String s = tagListView.getSelectionModel().getSelectedItem();

		if(s==null) {
			Alert noTagAlert = new Alert(AlertType.INFORMATION);
			noTagAlert.initOwner(Photos.primaryStage);
			noTagAlert.setTitle("Unable to Perform Function");
			noTagAlert.setHeaderText("No tags to delete.");
			noTagAlert.showAndWait();
			return;
		}
		if(tagList.size()==0) {
			Alert noTagAlert = new Alert(AlertType.INFORMATION);
			noTagAlert.initOwner(Photos.primaryStage);
			noTagAlert.setTitle("Unable to Perform Function");
			noTagAlert.setHeaderText("No tags to delete.");
			noTagAlert.showAndWait();
			return;
		}
		tagList.removeIf(tag -> tag.equals(s));
		tagList.forEach(tag -> System.out.println(tag));
		//obsTagList=FXCollections.observableArrayList(tagList);  
		obsTagList.removeIf(tag -> tag.equals(s));
		Collections.sort(obsTagList);
		tagListView.setItems(obsTagList);

		String[] splitString = s.split(" : ");
		selectedPhoto.deleteTag(new Tag(splitString[0], splitString[1]));
	}

	/**
	 * navigates to the slideshow for a given album.
	 * @param event
	 * @throws IOException
	 */
	public void toSlideshow(ActionEvent event) throws IOException {
		if(selectedPhotoAlbum.getAlbumNum() == 0) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Invalid Command");
			alert.setHeaderText("Cannot show slideshow. There are no photos in the album. Please add at least one photo in order to play the slideshow.");
			alert.showAndWait();
			return;
		}
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Slideshow.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.slideShowInitialization(Photos.primaryStage);
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();

	}

	/**
	 * intializes images for the slideshow as well as buttons and stuff for the page.
	 * @param primaryStage
	 */
	public void slideShowInitialization(Stage primaryStage) {


		slideshowAlbumLabel.setText(selectedPhotoAlbum.getName());

		slideshowImage.setImage(selectedPhoto.getImage());

		nextPhoto.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				ArrayList<Photo> photosInAlbum = selectedPhotoAlbum.getPhotosInAlbum();

				if(selectedPhotoAlbum.getAlbumNum() == 1) {
					return;
				}

				for(int i = 0; i < selectedPhotoAlbum.getAlbumNum(); i++) {
					if(photosInAlbum.get(i).equals(selectedPhoto)) {
						if(i == photosInAlbum.size() - 1) selectedPhoto = photosInAlbum.get(0);
						else selectedPhoto = photosInAlbum.get(i + 1);
						slideShowInitialization(Photos.primaryStage);
						return;
					}
				}


			}


		});

		previousPhoto.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				ArrayList<Photo> photosInAlbum = selectedPhotoAlbum.getPhotosInAlbum();

				if(selectedPhotoAlbum.getAlbumNum() == 1) {
					return;
				}

				for(int i = 0; i < selectedPhotoAlbum.getAlbumNum(); i++) {
					if(photosInAlbum.get(i).equals(selectedPhoto)) {
						if(i == 0) selectedPhoto = photosInAlbum.get(photosInAlbum.size() - 1);
						else selectedPhoto = photosInAlbum.get(i - 1);
						slideShowInitialization(Photos.primaryStage);
						return;
					}
				}


			}


		});


		exitSlideshow.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/PhotosInAlbum.fxml"));
				Parent newView;
				try {
					newView = (Parent)loader.load();
					Controller controller = loader.getController();
					controller.initializePhotosInAlbum(Photos.primaryStage);
					Scene newScene = new Scene(newView);

					Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

					window.setScene(newScene);
					window.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



			}
		});



	}


	//------------------------------------------------Search Album--------------------------------------------------
	/**
	 * Initializes the photos that were retrieved from the search.
	 * @param primaryStage
	 * @param listOfPhotos - list of the photos from the search query.
	 */
	public void initializePhotosInSearchPage(Stage primaryStage,ArrayList<Photo> listOfPhotos) {
		ArrayList<String> photoCaptionList = new ArrayList<String>();

		photoList = listOfPhotos;
		photoList.forEach(photo ->{

			photoCaptionList.add(photo.getCaption());
		});


		userNameLabel.setText(selectedUser.getName());

		obsPhotoList=FXCollections.observableArrayList(photoCaptionList);
		albumNameInAlbumLabel.setText("Searched Photo Album");




		//Collections.sort(obsPhotoList);


		photoListView.setItems(obsPhotoList);
		photoListView.setCellFactory(arg -> new ListCell<String>() {
			private ImageView i = new ImageView();
			@Override
			public void updateItem(String caption, boolean empty) {
				super.updateItem(caption, empty);
				if(empty) {
					setText(null);
					setGraphic(null);

				}
				else {
					for(Photo p : photoList) {
						if(p.getCaption().equals(caption)) {
							i.setImage(p.getImage());
							i.setFitHeight(80);
							i.setFitWidth(80);
							setText(p.getCaption());
							setGraphic(i);
						}
					}
				}
			}

		});


		photoListView.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) -> showPhotoItem(primaryStage)); 
		photoListView.getSelectionModel().select(0);

		logoutButton.setOnAction(arg0 -> {
			try {
				toLogin(arg0);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});

		backToAlbumsButton.setOnAction(arg0 -> {
			try {
				backToAlbums(arg0);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		makeAlbumFromPhotos.setOnAction(arg0 -> {
			try {
				makeAlbum(arg0,listOfPhotos);
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		});

		playSlideshowButton.setOnAction(arg0 -> {
			try {
				searchToSlideshow(arg0,listOfPhotos);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});


		copyPhotoButton.setOnAction(arg0 -> {
			try {
				copyPhoto(arg0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

	}

	/***
	 * Navigates to slideshow for the searched images
	 * @param event
	 * @param listOfPhotos - list of the photos from the search query.
	 * @throws IOException
	 */
	public void searchToSlideshow(ActionEvent event,ArrayList<Photo> listOfPhotos) throws IOException {
		if(listOfPhotos.size() == 0) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Invalid Command");
			alert.setHeaderText("Cannot show slideshow. There are no photos in the searched album.");
			alert.showAndWait();
			return;
		}
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Slideshow.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.searchSlideShowInitialization(Photos.primaryStage, listOfPhotos);
		Scene newScene = new Scene(newView);

		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(newScene);
		window.show();

	}

	/**
	 * Initializes slideshow for those images.
	 * @param primaryStage
	 * @param listOfPhotos - list of the photos from the search query.
	 */
	public void searchSlideShowInitialization(Stage primaryStage,ArrayList<Photo> listOfPhotos) {


		slideshowAlbumLabel.setText("");

		slideshowImage.setImage(selectedPhoto.getImage());

		nextPhoto.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				ArrayList<Photo> photosInAlbum = selectedPhotoAlbum.getPhotosInAlbum();

				if(selectedPhotoAlbum.getAlbumNum() == 1) {
					return;
				}

				for(int i = 0; i < selectedPhotoAlbum.getAlbumNum(); i++) {
					if(photosInAlbum.get(i).equals(selectedPhoto)) {
						if(i == photosInAlbum.size() - 1) selectedPhoto = photosInAlbum.get(0);
						else selectedPhoto = photosInAlbum.get(i + 1);
						slideShowInitialization(Photos.primaryStage);
						return;
					}
				}


			}


		});

		previousPhoto.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				ArrayList<Photo> photosInAlbum = selectedPhotoAlbum.getPhotosInAlbum();

				if(selectedPhotoAlbum.getAlbumNum() == 1) {
					return;
				}

				for(int i = 0; i < selectedPhotoAlbum.getAlbumNum(); i++) {
					if(photosInAlbum.get(i).equals(selectedPhoto)) {
						if(i == 0) selectedPhoto = photosInAlbum.get(photosInAlbum.size() - 1);
						else selectedPhoto = photosInAlbum.get(i - 1);
						slideShowInitialization(Photos.primaryStage);
						return;
					}
				}


			}


		});


		exitSlideshow.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/MakeAlbumFromSearch.fxml"));
				Parent newView;
				try {
					newView = (Parent)loader.load();
					Controller controller = loader.getController();
					controller.initializePhotosInSearchPage(Photos.primaryStage,listOfPhotos);
					Scene newScene = new Scene(newView);

					Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

					window.setScene(newScene);
					window.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



			}
		});



	}

	/**
	 * Creates and album from the images retrieved 
	 * @param event
	 * @param listOfPhotos - list of the photos from the search query.
	 */

	public void makeAlbum(ActionEvent event,ArrayList<Photo> listOfPhotos) throws Exception{

		if(listOfPhotos.size() == 0) {
			Alert noPhotosAlert = new Alert(AlertType.INFORMATION);
			noPhotosAlert.initOwner(Photos.primaryStage);
			noPhotosAlert.setTitle("Cannot Make Album");
			noPhotosAlert.setHeaderText("There are no photos to make an album.");
			noPhotosAlert.showAndWait();
			return;
		}
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle("Enter Album Name");

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);

		TextField name = new TextField();
		name.setPromptText("Album Name");

		gridPane.add(new Label("Name:"), 0, 0);
		gridPane.add(name, 1, 0);

		dialog.getDialogPane().setContent(gridPane);

		// Request focus on the username field by default.
		Platform.runLater(() -> name.requestFocus());

		// Convert the result to a username-password-pair when the login button is clicked.
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == loginButtonType) {
				return name.getText();
			}
			return null;
		});

		Optional<String> result = dialog.showAndWait();

		if(result.get().equals("")) {
			Alert noNameAlert = new Alert(AlertType.INFORMATION);
			noNameAlert.initOwner(Photos.primaryStage);
			noNameAlert.setTitle("Name cannot be empty");
			noNameAlert.setHeaderText("Please enter an album name.");
			noNameAlert.showAndWait();
			makeAlbum(event,listOfPhotos);
			return;
		}

		String albumName=result.get();
		
		ArrayList<PhotoAlbum> AlbumList = selectedUser.getAlbumList();
		AlbumList.forEach(album -> {
			
			if(album.getName().equals(albumName)) {
				Alert duplicateNameAlert = new Alert(AlertType.INFORMATION);
				duplicateNameAlert.initOwner(Photos.primaryStage);
				duplicateNameAlert.setTitle("Name cannot be empty");
				duplicateNameAlert.setHeaderText("Duplicate Album Name.");
				duplicateNameAlert.showAndWait();
				try {
					makeAlbum(event,listOfPhotos);
					return;
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}
			}
			
		});
		ArrayList<Photo> pList = new ArrayList<Photo>();
		for(Photo p : listOfPhotos) {

			ArrayList<Tag> newTags = new ArrayList<Tag>();
			newTags.addAll(p.getTags());
			Photo newP = new Photo(new ImageView(p.getImage()), p.getCaption(), p.getDate(), newTags);
			newP.setImagePath(p.getImagePath());
			pList.add(newP);

		}
		PhotoAlbum newAlbum=new PhotoAlbum(pList,albumName);
		selectedUser.addAlbum(newAlbum);


		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/NonAdminAlbums.fxml"));
		Parent newView = (Parent)loader.load();
		Controller controller = loader.getController();
		try {
			controller.openAlbumList(Photos.primaryStage);
			Scene newScene = new Scene(newView);

			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

			window.setScene(newScene);
			window.show();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




	}



	//Serialization for Users
	/**
	 * reads user from the Users.ser file with serializable
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void readUsers() throws IOException, ClassNotFoundException{
		try {
			FileInputStream fileInputStream = new FileInputStream("docs/Users.ser");
			BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
			ObjectInputStream objectInputStream = new ObjectInputStream(bufferedInputStream);
			userList = (ArrayList<User>)objectInputStream.readObject();
			objectInputStream.close();
		}
		//nothing in file
		catch(EOFException e) {
			userList = new ArrayList<User>();
		}

	}

	/**
	 * writes users to the users.ser with serializable.
	 * @throws IOException
	 */
	public static void writeUsers() throws IOException {

		FileOutputStream fileOutputStream = new FileOutputStream("docs/Users.ser");
		BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(bufferedOutputStream);
		objectOutputStream.writeObject(userList);
		objectOutputStream.close();

	}

}	
