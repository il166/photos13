package controllers;
//Itamar Levi, Young Choi


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
//import javafx.scene.layout.AnchorPane;
import javafx.stage.WindowEvent;
import java.io.*;
import java.util.*;

import model.*;



public class Photos extends Application implements Serializable{
	
	
	
		
	public static Stage primaryStage;
	public static Scene mainScene;
	public static FXMLLoader loader;
	
	/**
	 * starts the application
	 * @param primaryStage
	 */
    @Override
    public void start(Stage primaryStage) throws Exception{

    	
    	loader = new FXMLLoader(getClass().getResource("../view/LogInPage.fxml"));
    	Parent root = (Parent) loader.load();
    	    	
        Controller controller = loader.getController();
        controller.initialize(primaryStage);
        mainScene = new Scene(root);
        
        primaryStage.setScene(mainScene);
        primaryStage.setResizable(false);
        primaryStage.show();
       
        
        primaryStage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, arg0 -> {
			try {
				closeWindowEvent(arg0);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
    }
	
    
    /**
     * functionality of pressing X (close) button on application
     * @param event
     * @throws Exception
     */
    private void closeWindowEvent(WindowEvent event) throws Exception{
    
    	
       /* FileWriter file = new FileWriter("songs.json");
        file.write(obj.toJSONString());
        file.flush();
        */
        //file.close();
    	
    	try{
    		//Controller.writePhotoAlbumApp(Controller.albumList);
    		Controller.writeUsers();
    	}
    	catch(IOException ex) {
    		ex.printStackTrace();
    	}
    	

    }
 
    
	
	/**
	 * main - calls launch
	 * @param args
	 */
	public static void main(String[] args) {
		
		launch(args);
	
		
	}
	
	

	
	

}
